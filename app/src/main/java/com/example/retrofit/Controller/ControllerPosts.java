package com.example.retrofit.Controller;

import com.example.retrofit.Modell.Dao.Modell;
import com.example.retrofit.Modell.Pojo.Posts;
import com.example.retrofit.Utils.ResultListener;

import java.util.List;

public class ControllerPosts {

    Modell modell;

    public ControllerPosts() {
        this.modell = new Modell();
    }

    public void getResponse (final ResultListener<List<Posts>> escuchadorDeLaVista){
        ResultListener<List<Posts>> escuchadorDelControlador = new ResultListener<List<Posts>>() {
            @Override
            public void finish(List<Posts> results) {
                escuchadorDeLaVista.finish(results);
            }
        };
        modell.getPosts(escuchadorDelControlador);



    }
}
