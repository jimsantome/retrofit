package com.example.retrofit.Modell.Dao;

import android.util.Log;

import com.example.retrofit.Modell.Pojo.Posts;
import com.example.retrofit.Utils.JsonPlaceHolderApi;
import com.example.retrofit.Utils.ResultListener;
import com.example.retrofit.View.Adapters.RecyclerAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Modell {


    public Modell() {
    }

    public  void getPosts(final ResultListener<List<Posts>> escuchadorDelControlador){

        // declaramos de donde vamos a sacar la informacion
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        // le pasamos a la interface la info ??
        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);



        Call<List<Posts>> call = jsonPlaceHolderApi.getPosts();

        call.enqueue(new Callback<List<Posts>>() {
            @Override
            public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {
                if (!response.isSuccessful()){

                    Log.d("Tag","Error"+response.code());

                }

                escuchadorDelControlador.finish(response.body());


            }



            @Override
            public void onFailure(Call<List<Posts>> call, Throwable t) {

                Log.d("TAG","ERROR"+t.getMessage());


            }
        });


    }

}
