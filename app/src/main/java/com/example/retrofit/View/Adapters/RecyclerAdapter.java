package com.example.retrofit.View.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofit.Modell.Pojo.Posts;
import com.example.retrofit.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PostsViewHolder> {


    private List<Posts> listPosts;


    public RecyclerAdapter() {
        
        listPosts = new ArrayList<>();



    }

    @NonNull
    @Override
    public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();// rescatamos el contexto donde estamos parados;
        int layoutIdParaListaItem = R.layout.posts;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        boolean attachToParentRapido = false;
        View view = layoutInflater.inflate(layoutIdParaListaItem,parent,attachToParentRapido);//cargamos la vista

        //declaramos la clase de abajo , y le pasamos en el constructor la view

        PostsViewHolder postsViewHolder = new PostsViewHolder(view);

        return postsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PostsViewHolder holder, int position) {

        holder.userId.setText(listPosts.get(position).getUserId());
        holder.id.setText(listPosts.get(position).getId());
        holder.body.setText(listPosts.get(position).getBody());
        holder.title.setText(listPosts.get(position).getTitle());


    }

    @Override
    public int getItemCount() {
        return listPosts.size();
    }

    public void insertPosts(List<Posts> listaPosts){
        if(listaPosts != null){
            listPosts.clear();
            listPosts.addAll( listaPosts );
           ;
        }
    }



    class PostsViewHolder extends RecyclerView.ViewHolder {

        TextView id ;
        TextView userId;
        TextView title;
        TextView body;

        public PostsViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.titulo);
            userId = itemView.findViewById(R.id.userid);
            body = itemView.findViewById(R.id.body);


        }




        }
    }

