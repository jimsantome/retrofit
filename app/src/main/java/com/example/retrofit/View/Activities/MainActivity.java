package com.example.retrofit.View.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.retrofit.Controller.ControllerPosts;
import com.example.retrofit.Modell.Pojo.Posts;
import com.example.retrofit.R;
import com.example.retrofit.Utils.ResultListener;
import com.example.retrofit.View.Adapters.RecyclerAdapter;

import java.util.List;

import javax.xml.transform.Result;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRvListaPosts;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerAdapter recyclerAdapter;
    private ControllerPosts controllerPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        mRvListaPosts = findViewById(R.id.rvPosts);
        mRvListaPosts.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL));

        controllerPosts = new ControllerPosts();
        Toast.makeText(this, "LLego 1", Toast.LENGTH_SHORT).show();

        linearLayoutManager = new LinearLayoutManager(this);
        mRvListaPosts.setLayoutManager(linearLayoutManager);

        recyclerAdapter = new RecyclerAdapter();

        Toast.makeText(this, "llego2", Toast.LENGTH_SHORT).show();

        mRvListaPosts.setAdapter(recyclerAdapter);

        recibirPosts();

        Toast.makeText(this, "llego 3", Toast.LENGTH_SHORT).show();



    }

    public void recibirPosts(){
        controllerPosts.getResponse(new ResultListener<List<Posts>>() {
            @Override
            public void finish(List<Posts> results) {
                recyclerAdapter.insertPosts(results);
            }
        });
    }
}
